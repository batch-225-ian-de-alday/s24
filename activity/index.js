console.log(`Hello Ian`);

let x = 2
const getCube = Math.pow(x, 3);

console.log(`The cube of ${x} is ${getCube}.`);


let fullAddress =[258, `Washington Ave NW`, `California`, 90011];

let [HouseNumber, brgy, city, zipCode] = fullAddress;
console.log(`I live at ${HouseNumber} ${brgy}, ${city} ${zipCode}.`);


const animal = {
    name: "Lolong",
    type: "saltwater crocodile",
    weight: `1075 kg`,
    measurement: `20 ft and 3 inch`  
}

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed at ${weight} with a measurement of ${measurement}.`)


let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
    console.log(number);
})


let reduceNumbers = numbers.reduce((partialSum, a) => partialSum + a, 0);
console.log(reduceNumbers);

// const sum = [1, 2, 3].reduce((partialSum, a) => partialSum + a, 0);
// console.log(sum); // 6

/* let reduceNumbers = (num1, num2, num3, num4, num5) => {
    total = num1 + num2 + num3 + num4 + num5;
    console.log(total);
}

reduceNumbers(1,2,3,4,5); */


class Dog {
    constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
    }
}



myDog = new Dog(`Luffy`,2 ,`Siberian Husky`)
console.log(myDog);










